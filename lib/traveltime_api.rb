require 'rest-client'
require 'json'

# Class for using TravelTime API service. Learn more about TravelTime at
# www.traveltimeapp.com
#
# ## Defined types ##
#
# * TransportationMode - string with one of following values:
# "walking", "driving", "walking_train", "walking_bus", "public_transport"
#
# * EdgeType - describes type of the edge. String with one of following values:
# "car", "walk", "train", "bus", "cable_car", "plane", "ship"
#
# ## Thrown errors ##
#
# Each method that does a call to the server can raise TravelTimeAPI::BadRequest 
# or TravelTimeAPI::ServerError errors.
#
class TravelTimeAPI
  DEFAULT_URL = "http://api.traveltimeapp.com"
  OUT_OF_REACH_CODE = "out-of-reach"
  NO_DATA_CODE = "no-data"

  # Class representing authentification data for TravelTime API.
  class Auth < Struct.new(:app_id, :app_key)
    def to_h
      {:app_id => app_id, :app_key => app_key}
    end
  end

  # Raised if API user made bad request.
  class BadRequest < RuntimeError
    # Server provided String explanation of error.
    attr_reader :details

    def initialize(details)
      @details = details
    end

    def to_s
      "TravelTimeAPI::BadRequest[#{details}]"
    end
  end

  # Raised if API server has encountered an error.
  class ServerError < RuntimeError
    # Human readable error message.
    attr_reader :error, :http_code

    def initialize(error, http_code)
      @error = error
      @http_code = http_code
    end

    def to_s
      "TravelTimeAPI::ServerError[http code: #{@http_code} | #{@error}]"
    end
  end

  # Represents that API does has a dataset for this point/region but cannot
  # calculate any useful data from this point.
  class OutOfReach < RuntimeError; end

  # Represents that API does not have data for this point/region.
  class NoData < RuntimeError; end

  # Result of #time_filter method for one mode.
  #
  # Properties is a Hash that can have (if you requested them) following pairs:
  # * "time" => Fixnum - time from origin to point in seconds.
  # * "distance" => Fixnum - distance from origin to point in meters.
  #
  # @param points [Hash] {"id1" => Properties, ...}
  # @param accuracy [Symbol] :exact or :approx. Decrease travel time to switch
  # from approximate to exact accuracy.
  class TimeFilterResult < Struct.new(:points, :accuracy); end

  # Result of #time_map method.
  #
  # @param shape [Array] array of polylines that represent areas reachable by given time.
  # [Cluster, Cluster, ...], where Cluster is [[lat, lon], [lat, lon], ...]
  # @param accuracy [Symbol] :exact or :approx. Decrease travel time to switch
  # from approximate to exact accuracy.
  class TimeMapResult < Struct.new(:shape, :accuracy); end

  # Result of #routes method.
  #
  # * RouteDescriptionPart is a structure describing one part of the route:
  # {
  #   "mode": EdgeType,
  #   # Array of coordinates for this route part. Can be used to draw a polyline
  #   # on a map.
  #   "coords": [[lat, lon], [lat, lon], [lat, lon], ...],
  #   "directions: String, # textual description in english of this route part
  #   "distance": Int, # distance covered in metres for this route part
  #   "time": Int # time taken to travel through this route part in seconds
  # }
  #
  # @param routes [Hash] {"id1" -> [RouteDescriptionPart, RouteDescriptionPart, ...], ...}
  class RoutesResult < Struct.new(:routes); end

  # Initializes API. You need to specify your auth here.
  # 
  # Example:
  #   api = TravelTimeAPI.new(TravelTimeAPI::Auth.new(app_id, app_key))
  #
  def initialize(auth, url=DEFAULT_URL)
    @auth = auth
    @url = url
  end

  # Given latitude and longtitude of a point return whether TravelTime has 
  # data for those coordinates or not.
  #
  # WARNING: even though ending in ? this method does not return a boolean
  # value and you cannot use it directly in boolean expressions!
  #
  # This is due to the fact that is needs to return 3 possible outcomes:
  # - :data - we have data for this point
  # - :out_of_reach - we have a data set for this point but we couldn't attach
  # that point exactly to any significant roads or routes.
  # - :no_data - we do not have any data for this point.
  #
  # @param lat [Float]
  # @param lng [Float]
  # @return [Symbol]
  def has_data?(lat, lng)
    request = {:coords => [lat, lng]}
    response = raw_post("/has_data", request)
    if response["has"] == true
      :data
    elsif response["code"] == OUT_OF_REACH_CODE
      :out_of_reach
    elsif response["code"] == NO_DATA_CODE
      :no_data
    else
      json_error(response)
    end
  end

  # Takes input parameters and returns how long does it take to reach each point
  # in chosen mode of transportation (in seconds).
  #
  # Points whose travel times which exceed _travel_time_ are not included in the
  # result.
  #
  # @param start_time [Time] Time moment when we start our search.
  # @param travel_time [Int] Max time in seconds to the point.
  # @param modes [Array] Array of transportation modes you want calculated.
  #                      See class documentation for transportation mode info.
  # @param origin [Array] [latitude, longtitude]
  # @param points [Hash] Points th{"id1" => [latitude, longtitude], ...}
  # @param properties [Array] array of requested point properties. Valid values: :time,
  #                           :distance. :distance cannot be used with any mode that
  #                           involves public transport as the results will be 0.
  # @return [Hash] {mode => TravelTimeAPI::TimeFilterResult, ...}
  def time_filter(start_time, travel_time, modes, origin, points, properties=[:time])
    modes = Array(modes)
    request = {
      :start_time => format_time(start_time),
      :travel_time => travel_time,
      :modes => modes,
      :origin => origin,
      :points => points,
      :properties => properties
    }

    response = post("/v2/time_filter", request)
    response.inject({}) do |hash, (mode, ranked)|
      hash[mode] = TimeFilterResult.new(ranked["times"], ranked["accuracy"].to_sym)
      hash
    end
  end

  # Takes input parameters and returns polylines for each cluster that you can
  # reach by given time.
  #
  # @param start_time [Time] Time moment when we start our search.
  # @param travel_time [Int] Max time in seconds to the point.
  # @param mode [String] Transportation mode. See class documentation for info.
  # @param origin [Array] [latitude, longtitude]
  # @param smooth [Boolean] Shall the returned shaped be smoothed?
  # @param max_points [Fixnum] Maximum number of points in returned polygons. nil
  # for no limit, Fixnum for limit. Minimum of 4 points required!
  # @return [TravelTimeAPI::TimeMapResult]
  def time_map(start_time, travel_time, mode, origin, smooth, max_points=nil)
    raise ArgumentError.new("At least 4 points are required in max_points!") \
      unless max_points.nil? || max_points >= 4

    request = {
      :start_time => format_time(start_time),
      :travel_time => travel_time,
      :mode => mode,
      :origin => origin,
      :smooth => smooth,
      :max_points => max_points
    }

    response = post("/v2/time_map", request)
    TimeMapResult.new(response["shape"], response["accuracy"].to_sym)
  end

  # Takes input parameters and returns routes to each of the points within 
  # travel_time.
  #
  # Input parameters are the same as #travel_time.
  # @return [TravelTimeAPI::RoutesResult]
  def routes(start_time, travel_time, mode, origin, points)
    request = {
      :start_time => format_time(start_time),
      :travel_time => travel_time,
      :mode => mode,
      :origin => origin,
      :points => points
    }

    response = post("/v2/routes", request)
    RoutesResult.new(response)
  end

private

  def format_time(time)
    time.strftime("%FT%T%:z")
  end

  def raw_post(action, request)
    response = RestClient.post(
      @url + action,
      request.merge(@auth.to_h).to_json,
      :content_type => :json
    )
    JSON.parse(response)
  rescue RestClient::ExceptionWithResponse => e
    if e.http_code == 400
      raise BadRequest.new(e.http_body)
    else
      raise ServerError.new(
        e.http_code, "server error, http_body: #{e.http_body}"
      )
    end
  rescue JSON::ParserError => e
    raise ServerError.new("Cannot parse server response as JSON: #{response}")
  end

  def post(action, request)
    json = raw_post(action, request)

    case json["code"]
    when OUT_OF_REACH_CODE
      raise OutOfReach
    when NO_DATA_CODE
      raise NoData
    when nil
      json
    else
      json_error(json)
    end
  end

  def json_error(json)
    raise ServerError.new("code: #{json["code"]}, details: #{json["details"]}")
  end

end
