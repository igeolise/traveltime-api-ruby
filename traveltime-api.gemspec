# encoding: utf-8
@spec = Gem::Specification.new do |s|
  s.name = "traveltime-api"
  s.version = "1.4.2"
  s.summary = "Simple wrapper for TravelTime REST API."
  s.description = ""
  s.authors = ["Artūras Šlajus", "IGeolise"]
  s.email = ["arturas@igeolise.com"]
  s.homepage = "http://traveltimeapp.com"

  s.has_rdoc = true
  s.executables = %w[]
  s.add_dependency('rest-client', '>= 1.6.7')
  s.add_dependency('json', '>= 1.7.0')

  s.require_paths = %w[lib]
  s.files = Dir["lib/*"]
end
